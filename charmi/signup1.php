


<html>
<head>


	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  
	
    <link rel="stylesheet" href="css/bootstrap.min.css"  type="text/css">
	
	
    <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
  
	

    <link rel="stylesheet" href="css/style.css">
	


<title>Signup page</title>

</head>
<style type="text/css">
body{
		background-image:url("images/banner2.jpg");
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}



li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}


.wrapper,w3-container_demo{
	margin:auto;
	padding:15px;
	max-width:500px;
	width:450px;
	border:25px #000000;
	margin-top:25px;
	background-color:#000000
	
	}
</style>	
<body>
<header>
<ul>
   <li>
                        <a  href="about.html">About</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
					
                    <li>
                        <a  href="#contact">Contact</a>
                    </li>
  <li class="dropdown">
    <a href="web.html" class="dropbtn">My account</a>
    <div class="dropdown-content">
      <a href="#" style="color:orange">LOGIN</a>
      <a href="#" style="color:orange">SIGNUP</a>
     
    </div>
  </li>
</ul>

</header>


		<div class="w3-container_demo" style="margin-top:50px">
		
			<div class="wrapper">
			<div id="register" class="animate form">
                            <form  action="signup_process.php" method="post"> 
                                <h1 style="color:#00bcd4">Signup</h1>
                                <p> 
                                    <label for="username" class="uname" data-icon="u">Your username</label>
                                    <input id="username" name="username" required pattern="[a-zA-z]{3,15}$" type="text">
                                </p>
                                <p> 
                                    <label for="email" class="email" data-icon="e" > Your email</label>
                                    <input id="email" name="email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" type="email" > 
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p">Your password </label>
                                    <input id="passwords" name="password" required="required" type="password">
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                    <input id="passwordsignup_confirm" name="password2" required="required" type="password">
                                </p>
								<p> 
                                    <label for="mobileno" class="mobileno" data-icon="p">Mobile no </label>
                                    <input id="mobileno" name="mobileno" required pattern="[0-9]{10}" type="text">
                                </p>
                                <p class="signin button"> 
									<input type="submit" value="Sign up" name="signup_btn"/> 
								</p>
							</form>
			</div>
		</div>

</body>

</html>
